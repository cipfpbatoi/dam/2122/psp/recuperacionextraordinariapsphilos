package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio3;

public class Main {

    public static void main(String[] args) {

        /*
        Con el siguiente comando, podemos crear recorrer los
        caracteres ascii de la a la z minúscula

         */

        for ( int i = 97; i<=122; i++){
            LetterWriter lw = new LetterWriter((char)i);
        }

    }

}
