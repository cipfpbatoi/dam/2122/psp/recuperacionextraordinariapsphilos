# Recuperación extraordinaria de PSP

Cada resultado debe ir dentro de su módulo para cada desarrollo.

## Ejercicio 1 (2p)

En este ejercicio se pretende imprimir 10 veces cada vocal, de manera aleatoria y cuando se llegue al final,
asegurarnos de que las últimas letras son: "eiou". 

Para realizarlo se proporciona el código dentro del módulo Ejercicio1, si lo ejecutas, verás que el resultado es:

Resutado Actual:
*aaaaaaaaaaeeeeeeeeeeiiiiiiiiiioooooooooouuuuuuuuuu*

y el resultado esperado es **similar al dispuesto**:
*aeiouaeiouaoeiuoiaueoeuiaeuiaooieauiaeouiaeuoaeiou*

consideraciones:

* Utiliza la clase [LetterWriter](Ejercicio1/src/main/java/es/cipfpbatoi/psp/recuperacion/extraordinaria/ejercicio1/LetterWriter.java) como base para realizar todas las modificaciones
* Utiliza la clase `Thread` o la interfaz `Runnable` para generarlo, pero en cualquier caso, el hilo ha de tener el mismo nombre que el carácter
* Cada letra se imprime un número de veces y después de cada impresión se espera un tiempo concetro. Todos esos valores pueden cambiarse si es necesario. 

> Del total de los 50 caracteres impresos, los 46 primeros caracteres pueden ir en cualquier orden, pero obligatoriamente, los cuatro últimos han de ser **eiou** (puede que salga antes la a siempre, pero no tiene por qué)

Puedes ver un ejemplo de la salida esperada en: [ej1_salida_ejemplo.txt](Ejercicio1/src/main/resources/ej1_salida_ejemplo.txt)

## Ejercicio 2 (3p)

En este ejercicio se ha de partir del supuesto anterior ( aunque no es necesario que el final de la cadena impresa sea exactamente **eiou**) e imprimir información estadística de cuantas vocales de cada tipo se han impreso y cual ha sido el total de vocales.

Consideraciones:

* Se ha de utilizar un almacenamiento compartido entre todos los hilos de forma que almacene el valor de cada una de las vocales y que luego sea capaz de mostrar los estadísticos y el sumatorio total. Para ello, puede que sea útil el uso de un objeto de tipo `Map` como `HashMap`
* Si la información estadística de cada carácter se recoge en los hilos y no en el almacenamiento compartido, tendrá una penalización.

Puedes ver un ejemplo de ejecución en: [ej2_salida_ejemplo.txt](Ejercicio2/src/main/resources/ej2_salida_ejemplo.txt)

## Ejercicio 3 (4p)

En este ejercicio se pretende modelar la interrupción de hilos. Para ello, se va a partir de las clases anteriores y se va a modificar la clase `LetterWriter` de la siguiente manera: 

* En lugar de impimir todas letras en la misma línea, imprima una letra por línea.
* En lugar de crear un hilo por cada vocal, se va a crear un hilo por cada letra minúscula del abecedario, para ello puede utilizarse el código de ejemplo en Módulo [Main.java](Ejercicio3/src/main/java/es/cipfpbatoi/psp/recuperacion/extraordinaria/ejercicio3/Main.java).
* Utiliza como valor de espera entre iteración 500ms
* Imprime 10 letras de cada tipo
* ha de gestionar adecuadamente las interrupciones para que que informe y además deje de imprimir las letras.

Una vez estén hechas las modificaciones se ha de crear la clase `LetterKiller` que intentando matar secuencialmente a todas las letras. La clase tendrá las siguientes características:

* Probabilidad de acabar con una letra 0.75 sobre 1.
* Primero empezará por la letra `a`, si no lo consigue, lo volverá a intentar en el siguiente intento. Cuando lo consiga, pasará a la letra `b` y así sucesivamente.
* Numero de intentos 30.
* Tiempo entre intentos 100ms.
* Cuando arranque mostrará un mensaje de que ha empezado.
* Cuando finalize mostrará un mensaje como que ha finalizado.

Cuando termine la simulación se han de imprimir las cuentas para cada letra y el resultado total de letras impresas.

Si la información estadística de cada carácter se recoge en los hilos y no en el almacenamiento compartido, tendrá una penalización.

Puedes ver un ejemplo de ejecución en: [ej3_salida_ejemplo.txt](Ejercicio3/src/main/resources/ej3_salida_ejemplo.txt)

## Entrega

Se ha de entregar un zip/7zip/tar.gz del código desarrollado y el enlace al fork realizado con el código actualizado. En caso de que alguna falle se tomará para la corrección el directorio más reciente.
