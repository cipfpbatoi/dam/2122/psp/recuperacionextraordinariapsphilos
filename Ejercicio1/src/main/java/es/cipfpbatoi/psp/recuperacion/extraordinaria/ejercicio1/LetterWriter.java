package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio1;

public class LetterWriter {

    private final char character;
    private final int numberOfIterations;
    public final int waitMsBetweenIterations;
    public static final int DEFAULT_WAIT_MS = 100;
    public static final int DEFAULT_NUMBER_OF_ITERATIONS = 10;

    /**
     * Creates a new object of the class LetterWriter and doesn't launch the wait
     * @param character Character to Write also sets the thread name to this value
     */
    public LetterWriter(char character){
        this(character,
                DEFAULT_NUMBER_OF_ITERATIONS,
                DEFAULT_WAIT_MS);
    }

    /**
     * Creates a new object of the class LetterWriter, setting all the parameters
     *
     * @param character Character to Write also sets the thread name to this value
     * @param numberOfIterations Total iterations
     * @param waitMsBetweenIterations ms to wait between iterations
     *
     */
    public LetterWriter(char character, int numberOfIterations, int waitMsBetweenIterations)
    {
        this.character = character;
        this.numberOfIterations = numberOfIterations;
        this.waitMsBetweenIterations = waitMsBetweenIterations;
    }


    /**
     * Prints out the character a numberOfIterations and waitsMSBetweenIterations
     */
    public void printLetter() {

        for (int i = 0; i<numberOfIterations; i++){

            System.out.print(character);

            try {
                Thread.sleep(waitMsBetweenIterations);

            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }

    }

    @Override
    public String toString() {
        return String.valueOf(getCharacter());
    }

    /**
     *
     * @return The character to print
     */
    public char getCharacter() {
        return character;
    }

}
