package es.cipfpbatoi.psp.recuperacion.extraordinaria.ejercicio1;

public class Main {


    public static void main(String[] args) {

        LetterWriter a = new LetterWriter('a');
        LetterWriter e = new LetterWriter('e');
        LetterWriter i = new LetterWriter('i');
        LetterWriter o = new LetterWriter('o');
        LetterWriter u = new LetterWriter('u');

        System.out.println();

        a.printLetter();
        e.printLetter();
        i.printLetter();
        o.printLetter();
        u.printLetter();

        System.out.println();
    }
}
